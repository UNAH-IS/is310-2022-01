package main

import (
	"fmt"
	"math"
)

// Es una variable para contar el costo del algoritmo
var pasos int = 0

func esPrimo(n int) bool {
	if n > 0 && n <= 2 {
		return true
	}

	if n % 2 == 0 {
		return false
	}

	var limite int = int(math.Sqrt(float64(n)))

	for i := 3; i <= limite; i += 2 {
		pasos++
		if n % i == 0 {
			return false
		}
	} 	

	return true
}

func main() {
	var numero int
	fmt.Print("Escriba un numero: ")
	fmt.Scan(&numero)
	fmt.Println("Es primo?", esPrimo(numero))
	fmt.Println("Pasos:", pasos)
	porcentaje := float32(pasos) / float32(numero) * 100.0
	fmt.Println("Reduccion %:", porcentaje)

}

/*

n = 1,000,003	iteraciones = 499
3..n^(1/2) avanza de 2 en 2

f(n) ~ n^(1/2) / 2 ~ 1/2 * n^(1/2)
O(n^(1/2))

0..n (avanza de 2 en 2)
f(n) ~ n/2
= O(n)
*/