package main

import "fmt"

func main() {
	// Es un array -> tamaño fijo
	numeros := [...]int{20, 30, 5, 2, 1, 4, 10, 23, 45, 67}

	// Es un slice -> tamaño dinámico
	//numeros := []int{20, 30, 5, 2, 1, 4, 10, 23, 45, 67}

	//fmt.Printf("%v del tipo %T\n", numeros, numeros)

	fmt.Println("Escriba un numero entero...")
	var numero int
	fmt.Scan(& numero)
	fmt.Println("Se leyo el valor", numero)

	var fue_hallado bool  // false
	var pasos int  // 0
	
	// foreach
	for i, valor := range numeros {
		pasos += 1
		if valor == numero {
			fmt.Println("Se encontro en la posicion:", i)
			fue_hallado = true
			break
		}
	}

	if !fue_hallado {
		fmt.Println("No se encontro el numero: ", numero)
	}
	fmt.Printf("Se hicieron %v pasos", pasos)
}