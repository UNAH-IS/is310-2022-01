package main

import (
	"fmt"
	str "strings"
)

func conteoSubcadenas(cadena, subcadena string) (int) {
	n := len(cadena)
	m := len(subcadena)

	if n < m || n == 0 {
		return 0
	}

	var conteo int
	// n - m + 1
	for i := 0; i < n - m + 1; i++ {
		// m + i - i = m
		if subcadena == cadena[i : i + m] {
			conteo += 1
		}
	}
	return conteo
}

func main() {
	s := "abababa"
	t := "aba"

	c := conteoSubcadenas(s, t)
	fmt.Println(c)

	fmt.Print(str.Count(s, t))
}