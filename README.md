# IS310-2022-01

Repositorio de codigo de IS-310 para el I PAC de 2022

## Temas
### Parcial 1
- [Elementos basicos](https://gitlab.com/UNAH-IS/is310-2022-01/-/tree/main/basico)
- [Analisis del Big-O para los bucles](https://gitlab.com/UNAH-IS/is310-2022-01/-/tree/main/analisis)
- [Funciones](https://gitlab.com/UNAH-IS/is310-2022-01/-/tree/main/funciones)
- [Punteros](https://gitlab.com/UNAH-IS/is310-2022-01/-/tree/main/puntero)

### Parcial 2
- [Struct](https://gitlab.com/UNAH-IS/is310-2022-01/-/tree/main/struct)
- [Genéricos](https://gitlab.com/UNAH-IS/is310-2022-01/-/tree/main/genericos): Permitir que funciones o estructuras permitan el uso de varios tipos de datos.
- [Calculadora](https://gitlab.com/UNAH-IS/is310-2022-01/-/tree/main/calculadora): Como ejemplo de un modulos.

#### TDA
- [LinkedList](https://gitlab.com/UNAH-IS/is310-2022-01/-/tree/main/linkedlist)
- [Stack](https://gitlab.com/UNAH-IS/is310-2022-01/-/tree/main/stack)
- [Queue](https://gitlab.com/UNAH-IS/is310-2022-01/-/tree/main/queue)

## Parcial 3
- [Recursión](https://gitlab.com/UNAH-IS/is310-2022-01/-/tree/main/recursion)
- Árboles n-arios
- Árboles binarios
- Grafos
---

## Como usar el repositorio
Clonar el proyecto: 
```
git clone https://gitlab.com/UNAH-IS/is310-2022-01.git
```

Desde la terminal acceder al directorio del proyecto
```
cd is310-2022-01/
```

El proyecto posee varios sub-directorios y existen 2 tipos de éstos:
- Si poseen un archivo `go.mod` son módulos, por lo que debe buscar el archivo `main.go` para ejecutarlo. 
- Si no poseen el archivo, todos los archivos puede ejecutarse directamente ya que todos poseen una función **main**.

Para ejecutar un archivo se utiliza el comando: 
```
go run archivo.go
```

Es posible ejecutar el archivo desde el directorio que lo aloja: 
```
// Windows
go run directorio\archivo.go

// Otros SO
go run directorio/archivo.go
```
Se puede utilizar el plugin *Code Runner*. Pero de existir un módulo puede haber errores en su ejecución.