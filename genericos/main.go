/*

En el siguiente ejemplo se muestra una de las utilidades de interface como
un metodo para generar soluciones genericas.

*/

package main

import "fmt"

func test(x interface{}) {
	fmt.Println(x)
}

type Persona struct {
	nombre string
}

func main() {
	test(true)
	test(10)
	test(10.4)
	test("hola")
	test(Persona{nombre: "Jose"})
}