## Conversion
Para transformar un `interface` a otro tipo se utiliza el siguiente esquema:
```
var x interface{} = 7

// Para pasar dicho valor de nuevo a int
var y int = x.(int)
```