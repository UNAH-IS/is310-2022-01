/*

En esta version el info del nodo es un Termino del polinomio

*/
package main

import (
	"fmt"
	"math"
	LL "gitlab.com/UNAH-IS/estructuras/linkedlist"
)

type Termino struct {
	coeficiente int
	exponente int
}

func (t Termino) Calcular(x float64) float64 {
	return float64(t.coeficiente) * math.Pow(x, float64(t.exponente))
}

func (t Termino) String() string {
	return fmt.Sprintf("%dX^%d", t.coeficiente, t.exponente)
}

// Agregar una funcionalidad a la lista enlazada
func GetTabla(list LL.ListaSimple) {
	var valores []float64 = []float64 {0, 0.5, 1, 1.5, 2, 2.5, 3, 3.5, 4, 4.5, 5}

	if list.EstaVacia() {
		return
	}

	for _, x := range valores {
		var suma float64 = 0
		aux := list.Primero

		for aux != nil {
			termino, ok := aux.Info.(Termino)

			if ok {
				suma += termino.Calcular(x)
			}

			aux = aux.Sig
		}
		fmt.Printf("f(%v) = %v\n", x, suma)

	}

}

func main() {
	t1 := Termino {coeficiente: 3, exponente: 4}
	t2 := Termino {coeficiente: -4, exponente: 2}
	t3 := Termino {coeficiente: 11, exponente: 0}


	lista := LL.ListaSimple{}
	lista.AgregarUltimo(t1)
	lista.AgregarUltimo(t2)
	lista.AgregarUltimo(t3)

	fmt.Println(lista)
	GetTabla(lista)
}