# Pila (stack)

Esta estructura es un LIFO (Last In - First Out) lo que indica que los elementos
recientes son los primeros (están en la superficie). El primer elemento de una
pila es la cima (top).

## Estructura

```
Cima -> [10]  (Insertar, eliminar)
        [20]
        [30]
        [40]
```

## Operaciones
- **push**: Permite agregar un elementos en la pila y lo establece como nueva cima.
- **pop**: Remueve la cima actual y la retorna (al menos su valor). Luego, cambia la cima por el elemento que esta al inicio.

## Implementación
En este caso se hará uso de una Lista Enlazada Simple donde:  `Nodo primero -> cima`. Por lo que, insertar al primero será nuestro push() y eliminar al primero el pop(). Finalmente para mostrar la cima(top()) se hará a través del retorno del primer nodo.