package stack

import "testing"

func TestPilaVacia(t *testing.T) {
	pila := NewPila()
	_, err := pila.Top()

	if err == nil {
		t.Errorf("Debio generarse un error")
	}
}

func TestPush(t *testing.T) {
	pila := NewPila()

	pila.Push(10)
	pila.Push(20)

	cima, err := pila.Top()

	if err != nil {
		t.Error("Error inesperado: ", err)
	}

	if cima.(int) != 20 {
		t.Errorf("Esperando 20; obtenido %v", cima)
	}
}

func TestPop(t *testing.T) {
	pila := NewPila()

	pila.Push(10)
	pila.Push(20)

	eliminado, err := pila.Pop()

	if err != nil {
		t.Error("Error inesperado: ", err)
	}

	if eliminado.(int) != 20 {
		t.Errorf("Esperado 20; eliminado %v", eliminado)
	}

	eliminado, err = pila.Pop()

	if err != nil {
		t.Error("Error inesperado: ", err)
	}
	
	if eliminado.(int) != 10 {
		t.Errorf("Esperado 10; eliminado %v", eliminado)
	}

	_, err = pila.Pop()

	if err == nil {
		t.Error("Debio ocurrir un error")
	}
}