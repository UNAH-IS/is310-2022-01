package stack

import (
	"errors"
	LL "tda/linkedlist"
)

type Pila struct {
	lista *LL.ListaSimple
}

func NewPila() *Pila {
	p := new(Pila)
	p.lista = LL.NewListaSimple()
	return p
}

func(p *Pila) Push(info interface{}) {
	p.lista.AgregarPrimero(info)
}

func (p *Pila) Pop() (interface{}, error) {
	result, err := p.lista.RemoverPrimero()

	// Cambiar el mensaje de error por uno mas apropiado
	if err != nil {
		err = errors.New("pila vacia")
	}

	return result, err
}

func (p Pila) Top() (info interface{}, err error) {
	info, err = p.lista.GetPrimero()

	if err != nil {
		err = errors.New("pila vacia")
	}

	return
}

func (p Pila) String() string {
	return p.lista.String()
}