package queue

import "testing"

func TestColaVacia(t *testing.T) {
	cola := NewCola()
	_, err := cola.Frente()

	if err == nil {
		t.Errorf("Debio generarse un error")
	}
}

func TestEncolar(t *testing.T) {
	cola := NewCola()

	cola.Encolar(10)
	cola.Encolar(20)

	frente, err := cola.Frente()

	if err != nil {
		t.Error("Error inesperado: ", err)
	}

	if frente.(int) != 10 {
		t.Errorf("Esperando 10; obtenido %v", frente)
	}
}

func TestDesencolar(t *testing.T) {
	cola := NewCola()

	cola.Encolar(10)
	cola.Encolar(20)

	fin, err := cola.Desencolar()

	if err != nil {
		t.Error("Error inesperado: ", err)
	}

	if fin.(int) != 10 {
		t.Errorf("Esperado 10; eliminado %v", fin)
	}

	fin, err = cola.Desencolar()

	if err != nil {
		t.Error("Error inesperado: ", err)
	}
	
	if fin.(int) != 20 {
		t.Errorf("Esperado 20; eliminado %v", fin)
	}

	_, err = cola.Desencolar()

	if err == nil {
		t.Errorf("Debio ocurrir un error")
	}

}