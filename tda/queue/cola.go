package queue

import (
	"errors"
	LL "tda/linkedlist"
)

type Cola struct {
	lista *LL.ListaSimple
}

func NewCola() *Cola {
	c := new(Cola)
	c.lista = LL.NewListaSimple()
	return c
}

func (c *Cola) Encolar(info interface{}) {
	c.lista.AgregarUltimo(info)
}

func (c *Cola) Desencolar() (info interface{}, err error) {
	info, err = c.lista.RemoverPrimero()

	if err != nil {
		err = errors.New("cola vacia")
	}

	return
}

func (c Cola) Frente() (info interface{}, err error) {
	info, err = c.lista.GetPrimero()

	if err != nil {
		err = errors.New("cola vacia")
	}

	return
}

func (c Cola) String() string {
	return c.lista.String()
}