# Cola (Queue)
Esta estructura es un FIFO (First in - First out). Esto significa que los elementos más recientes se van relegando al final mientras que los más antiguos son los que se remuevan primero.

## Estructura
```
  Frente -> [10] (Eliminar)
            [20]
            [30]
     Fin -> [40] (Insertar)
```

## Operaciones
Las operaciones básicas son:
- **encolar (enqueue)**: Coloca un nuevo nodo al final de la cola.
- **desencolar (dequeue)**: Remueve el nodo que está al frente. Generando un nuevo frente.

## Implementación
En este caso se hará uso de una Lista Enlazada Simple donde: 
- Nodo primero -> frente.
- Nodo último -> fin.
