package linkedlist

import "testing"

func TestListaVacia(t *testing.T) {
	lista := &ListaSimple {}

	if lista.Size() != 0 {
		t.Errorf("Size() = %v; debe ser 0", lista.Size())
	}

	if !lista.EstaVacia() {
		t.Errorf("EstaVacia() = %v; debe ser true", lista.EstaVacia())
	}
}

func TestListaInsertar(t *testing.T) {
	list := &ListaSimple{}

	list.AgregarPrimero(10)
	list.AgregarPrimero(20)

	if list.Size() != 2 {
		t.Errorf("Tiene %d nodos; deben ser 2", list.Size())
	}

	var primero int = list.Primero.Info.(int)
	var ultimo int = list.Ultimo.Info.(int)

	if primero != 20 {
		t.Errorf("Primero == %v; debe ser 20", primero)
	}

	if ultimo != 10 {
		t.Errorf("Ultimo == %v; debe ser 10", ultimo)
	}

	list.AgregarUltimo(30)
	primero = list.Primero.Info.(int)
	ultimo = list.Ultimo.Info.(int)

	if primero != 20 {
		t.Errorf("Primero == %v; debe ser 20", primero)
	}

	if ultimo != 30 {
		t.Errorf("Ultimo == %v; debe ser 30", ultimo)
	}
}