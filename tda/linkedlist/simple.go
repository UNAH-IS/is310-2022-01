package linkedlist

import (
	"bytes"
	"errors"
	"fmt"
)

/*******************************************************************************
 *
 *							NODO
 *
 ******************************************************************************/

// Estructura Nodo
type Nodo struct {
	Info interface{}
	Sig *Nodo
}

func (n Nodo) String() string {
	// return strconv.Itoa(n.Info)
	return fmt.Sprintf("[%v]->", n.Info)
}

/*******************************************************************************
 *
 *							LISTA ENLAZADA
 *
 ******************************************************************************/

type ListaSimple struct {
	// Declaracion de los dos unicos nodos que conocemos directamente
	Primero *Nodo
	Ultimo *Nodo
	// Cantidad de nodos
	tamaño int
}

/* 
Este "constructor" no tiene mucha utilidad ya que inicializa los valores 
similares a los valores por defecto, pero sirve para establecer una funcion
que puede servir para inicializar un objeto.
*/
func NewListaSimple() *ListaSimple {
	list := new(ListaSimple)
	list.Primero = nil
	list.Ultimo = nil
	list.tamaño = 0
	return list
}

func (lista ListaSimple) Size() int {
	return lista.tamaño
}

func (lista ListaSimple) EstaVacia() bool {
	// Como saber que una lista esta vacia? R/ si Primero == nil
	// tamaño == 0
	return lista.Primero == nil
}

func (lista *ListaSimple) AgregarPrimero (Info interface{}) {
	nuevo := Nodo {
		Info: Info,
		Sig: nil,
	}

	if lista.EstaVacia() {
		lista.Ultimo = &nuevo
	} else {
		nuevo.Sig = lista.Primero
	}

	lista.Primero = &nuevo
	lista.tamaño += 1
}


func (list *ListaSimple) AgregarUltimo (Info interface{}) {
	nuevo := Nodo{
		Info: Info,
		Sig: nil,
	}

	if (list.EstaVacia()) {
		list.Primero = &nuevo
	} else {
		list.Ultimo.Sig = &nuevo
	}

	list.Ultimo = &nuevo
	list.tamaño += 1
}

func (list *ListaSimple) RemoverPrimero() (Info interface{}, err error) {
	if list.EstaVacia() {
		err = errors.New("lista vacia")
	} else {
		aux := list.Primero
		list.Primero = list.Primero.Sig
		Info = aux.Info
	}
	list.tamaño -= 1
	return
}

func (list *ListaSimple) RemoverUltimo() (interface{}, error) {
	if (list.EstaVacia()) {
		return 0, errors.New("lista vacia")
	}

	aux := list.Primero
	Info := list.Ultimo.Info

	// Llegar hasta el penUltimo
	for aux.Sig != list.Ultimo {
		aux = aux.Sig
	}

	aux.Sig = nil
	list.Ultimo = aux
	list.tamaño -= 1

	return Info, nil
}

func (list ListaSimple) Buscar(valor interface{}) (*Nodo) {
	if (list.EstaVacia()) {
		return nil
	}

	aux := list.Primero

	for (aux != nil) {
		if (aux.Info == valor) {
			return aux
		}
		aux = aux.Sig
	}

	return nil
}

func (list ListaSimple) GetPrimero() (interface{}, error) {
	if list.EstaVacia() {
		return nil, errors.New("lista vacia")
	}

	return list.Primero.Info, nil
}

func (list ListaSimple) GetUltimo() (interface{}, error) {
	if list.EstaVacia() {
		return nil, errors.New("lista vacia")
	}

	return list.Ultimo.Info, nil
}

func (list ListaSimple) String() string {
	var b bytes.Buffer

	if !list.EstaVacia() {
		aux := list.Primero

		for aux != nil {
			b.WriteString(aux.String())
			aux = aux.Sig
		}
	}

	return b.String()
}
