# Lista Enlazada
## Representacion
Un nodo por lo general tiene:
- Datos
- Enlace al siguiente nodo

`[info | sig]`

## Representacion de la Lista Enlazada
`[10]->[20]->[30]->[40]->`

Una lista enlazada tiene al menos los siguientes componentes:
- Enlace al primer nodo: 10
- Enlace al ultimo nodo: 40

## Funciones principales
- AgregarPrimero -> O(1)
- AgregarUltimo -> O(1)
- RemoverPrimero -> O(1)
- RemoverUltimo -> O(n)
- Buscar -> O(n)
- Verificar si la lista esta vacia (0 nodos) -> O(1)