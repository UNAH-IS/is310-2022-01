# TDA
Conjunto de estructuras básicas. Basado en la descripción del libro de texto de Joyanes.


## Test unitarios
Para ejecutar los tests de cada TDA utilizando el comando. Con esto puede verificar que los TDA funcionan correctamente.
```
go test -v ./linkedlist
go test -v ./stack
go test -v ./queue
```

## Importar paquete
Para utilizar los paquetes al importar el módulo y los posibles paquetes:
```
import gitlab.com/UNAH-IS/is310-2022-01/tda/[paquete]
```
Los paquetes disponibles son:
  - linkedlist
  - stack
  - queue

Para utilizarlos es necesario establecer un módulo para el proyecto con:
```
go mod init [proyecto]
```

Y una vez se establezca los imports necesarios:
```
go mod tidy
```
