package main

import "fmt"

func a() {
	b()
	fmt.Println("a()")
}

func b() {
	c()
	fmt.Println("b()")
}

func c() {
	fmt.Println("c()")
}

func main() {
	a()
	fmt.Println("main()")
}