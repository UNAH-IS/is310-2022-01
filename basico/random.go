package main

import (
    "fmt"
    "math/rand"
    "time"
)

func main() {
    // [var] [nombre de la variable] [tipo] = [valor]
    var semilla int64 = time.Now().UnixMilli()       
    rand.Seed(semilla)
    fmt.Println("Numero pseudo-aleatorio:", rand.Intn(10))    
}

// convertir un int32 -> int64