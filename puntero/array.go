package main

import "fmt"

func main() {
	var arr [5]int = [...]int{10, 20, 30, 40, 50}
	var pArr *[5]int = &arr

	// Modificando el valor del primer elemento de un array.
	(*pArr)[0] = 200
	fmt.Println(*pArr)
}