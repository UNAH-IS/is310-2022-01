package main

import "fmt"

func main() {
	var x int = 156

	// p esta apuntando a la direccion de memoria de x
	// ya que [x] es un int [p] debe ser int, tambien.
	var p *int = &x

	// Donde esta ubicado en memoria p
	fmt.Println("Direccion de memoria del puntero:", &p)
	// Donde esta ubicado en memoria x
	fmt.Println("Direccion de memoria de su referencia: ", p)
	// El valor de x
	fmt.Println("Valor de la referencia:", *p)

	// Modificando el valor al que apunta p
	*p = 200
	fmt.Println("El puntero puede modificar a la variable a la que referencia")
	fmt.Println("Ahora el valor de X (156 o 200)? ", x)
}