package main

import "fmt"


func main() {
	var x int = 156
	var y float32 = 3.4

	fmt.Println("VARIABLE X")
	fmt.Printf("Valor: %v y su tipo %T\n", x, x)
	fmt.Printf("Direccion en memoria: %v y su tipo %T\n", &x, &x)
	
	fmt.Println("VARIABLE Y")
	fmt.Printf("Valor: %v y su tipo %T\n", y, y)
	fmt.Printf("Direccion en memoria: %v y su tipo %T\n", &y, &y)
}