package main

import (
    "fmt"
    "math"
)

type Circulo struct {
	radio float64
}

func (c Circulo) area() float64 {
	return math.Pi * math.Pow(c.radio, 2)
}

type Rectangulo struct {
	ancho, alto float64
}

func (r Rectangulo) area() float64 {
	return r.ancho * r.alto
}

type Forma interface {
	area() float64
}

func medirArea(miForma Forma) {
	fmt.Println(miForma)
	fmt.Println(miForma.area())
	fmt.Println("----------")
}

func main() {
	rect := Rectangulo {
		ancho: 10.0,
		alto: 20.0,
	}

	circ := Circulo {
		radio: 10.0,
	}

	medirArea(circ)
	medirArea(rect)
	
}