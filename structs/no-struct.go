package main

import (
	"fmt"
	"math"
)

func areaCirculo(r float64) float64 {
	return math.Pi * math.Pow(r, 2)
}

func perimetroCirculo(r float64) float64 {
	return math.Pi * 2 * r
}

func main() {
	radio := 5.0
	fmt.Println("Area: ", areaCirculo(radio), "u^2")
	fmt.Println("Perimetro: ", perimetroCirculo(radio), "u")

}