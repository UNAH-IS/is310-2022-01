package main

import (
	"fmt"
	"time"
)


type Persona struct {
	nombre string
	genero string
	fecha_nacimiento time.Time
	identidad string
}

func (p *Persona) getFechaNacimiento() string {
	return p.fecha_nacimiento.Format("2006-02-01")
}

type Alumno struct {
	persona Persona
	carrera string
	cuenta string
}

func main() {
	ana := Persona{
		nombre: "Ana",
		genero: "Femenino",
		identidad: "0801200001234",
		fecha_nacimiento: time.Date(2000, time.May, 5, 14, 45, 0, 0, time.UTC),
	}
	fmt.Println(ana.getFechaNacimiento())

	alumnoIngenieria := Alumno{
		persona: ana,
		carrera: "Ingenieria Electrica",
		cuenta: "20181000011",
	}

	fmt.Println(alumnoIngenieria)
	fmt.Println(alumnoIngenieria.persona.getFechaNacimiento())
}