package main

import (
	"fmt"
	"math"
)


type Circulo struct {
	// Punto de origen
	x float64
	y float64

	radio float64
}

// Metodo de una estructura
func (c *Circulo) area() float64 {
	radio := (*c).radio
	return math.Pi * math.Pow(radio, 2)
}

func main() {
	// Declaracion
	var c01 *Circulo 
	// Inicializacion
	c01 = new(Circulo)
	c02 := Circulo{x: 10, y: 5, radio: 5}
	c03 := &c02

	(*c03).radio = 1

	fmt.Printf("Circulo 1: %v [%T]\n", *c01, *c01)
	fmt.Printf("Circulo 2: %v [%T]\n", c02, c02)
	fmt.Printf("Circulo 3: %v [%T]\n", c03, c03)

	fmt.Println("Area del c02: ", c02.area())
}