## Esquema de una estructura
```
type Alumno struct {
    cuenta string
    nota int
}
```

Para crear un objeto del struct:
```
x := Alumno {
    cuenta: "20221013452",
    nota: 65
}
```

Otra alternativa:
```
var y *Alumno := new(Alumno)
(*y).cuenta = "20221013452"
(*y).nota = 65
```

## Metodos
Un metodo es una funcion que se le agregan los receptores (objetos) del struct. El receptor se puede pasar por valor o por referencia (punteros). Los segundos permiten modificar al receptor mismo.
```
func (receptores) nombre (parametros) (retornos) {
...
}
```